<?php

use yii\db\Migration;

/**
 * Class m180110_034719_default
 */
class m180110_034719_default extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
		$this->execute("

CREATE TABLE IF NOT EXISTS `perabot` (
  `idperabot` int(11) NOT NULL,
  `nama` varchar(45) NOT NULL,
  `harga` int(11) NOT NULL,
  `idproperti` int(11) NOT NULL,
  `iduser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `properti` (
  `idproperti` int(11) NOT NULL,
  `nama` varchar(45) NOT NULL,
  `luas` varchar(45) NOT NULL,
  `harga` int(11) NOT NULL,
  `iduser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `user` (
  `iduser` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `password` varchar(45) DEFAULT NULL,
  `rule` enum('Admin','Manager','Sales') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `perabot`
  ADD PRIMARY KEY (`idperabot`),
  ADD KEY `idproperti` (`idproperti`),
  ADD KEY `iduser` (`iduser`);

ALTER TABLE `properti`
  ADD PRIMARY KEY (`idproperti`),
  ADD UNIQUE KEY `iduser` (`iduser`);

ALTER TABLE `user`
  ADD PRIMARY KEY (`iduser`);

ALTER TABLE `perabot`
  MODIFY `idperabot` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `properti`
  MODIFY `idproperti` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `user`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `perabot`
  ADD CONSTRAINT `perabot_ibfk_1` FOREIGN KEY (`idproperti`) REFERENCES `properti` (`idproperti`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `perabot_ibfk_2` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `properti`
  ADD CONSTRAINT `properti_ibfk_1` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

			");

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180110_034719_default cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180110_034719_default cannot be reverted.\n";

        return false;
    }
    */
}
