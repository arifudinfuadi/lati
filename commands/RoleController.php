<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use Yii;
use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class RoleController extends Controller {

    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    
    public function actionInit() {
        $auth = Yii::$app->authManager;
        $auth->removeAll();

        $createPost = $auth->createPermission('createPost');
        $auth->add($createPost);
        
        $viewPost = $auth->createPermission('viewPost');
        $auth->add($viewPost);
        $auth->addChild($createPost, $viewPost);
        
        $updatePost = $auth->createPermission('updatePost');
        $auth->add($updatePost);
        
        $sales = $auth->createRole('sales');
        $auth->add($sales);
        $auth->addChild($sales, $viewPost);
        
        $manager = $auth->createRole('manager');
        $auth->add($manager);
        $auth->addChild($manager, $createPost);
        $auth->addChild($manager, $sales);
        
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $createPost);
        $auth->addChild($admin, $updatePost);
        $auth->addChild($admin, $manager);
        $auth->addChild($admin, $sales);
        
    }

}

