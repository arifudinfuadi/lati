<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "perabot".
 *
 * @property int $idperabot
 * @property string $nama
 * @property int $harga
 * @property int $idproperti
 * @property int $iduser
 *
 * @property Properti $properti
 * @property User $user
 */
class Perabot extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'perabot';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'harga', 'idproperti', 'iduser'], 'required'],
            [['harga', 'idproperti', 'iduser'], 'integer'],
            [['nama'], 'string', 'max' => 45],
            [['idproperti'], 'exist', 'skipOnError' => true, 'targetClass' => Properti::className(), 'targetAttribute' => ['idproperti' => 'idproperti']],
            [['iduser'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['iduser' => 'iduser']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idperabot' => 'Idperabot',
            'nama' => 'Nama',
            'harga' => 'Harga',
            'idproperti' => 'Idproperti',
            'iduser' => 'Iduser',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperti()
    {
        return $this->hasOne(Properti::className(), ['idproperti' => 'idproperti']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['iduser' => 'iduser']);
    }
}
