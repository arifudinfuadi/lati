<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Properti */

$this->title = 'Update Properti:'.$model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Propertis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idproperti, 'url' => ['view', 'id' => $model->idproperti]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="properti-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
