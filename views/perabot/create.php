<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Perabot */

$this->title = 'Create Perabot';
$this->params['breadcrumbs'][] = ['label' => 'Perabots', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="perabot-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
