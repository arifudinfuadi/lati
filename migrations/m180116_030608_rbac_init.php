<?php

use yii\db\Migration;

/**
 * Class m180116_030608_rbac_init
 */
class m180116_030608_rbac_init extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();

        $createPost = $auth->createPermission('createPost');
        $auth->add($createPost);
        
        $viewPost = $auth->createPermission('viewPost');
        $auth->add($viewPost);
        $auth->addChild($createPost, $viewPost);
        
        $updatePost = $auth->createPermission('updatePost');
        $auth->add($updatePost);
        
        $sales = $auth->createRole('sales');
        $auth->add($sales);
        $auth->addChild($sales, $viewPost);
        
        $manager = $auth->createRole('manager');
        $auth->add($manager);
        $auth->addChild($manager, $createPost);
        $auth->addChild($manager, $sales);
        
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $updatePost);
        $auth->addChild($admin, $manager);
        $auth->addChild($admin, $sales);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180116_030608_rbac_init cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180116_030608_rbac_init cannot be reverted.\n";

        return false;
    }
    */
}
