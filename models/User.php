<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $iduser
 * @property string $name
 * @property string $password
 * @property string $rule
 *
 * @property Perabot[] $perabots
 * @property Properti $properti
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'rule'], 'required'],
            [['rule'], 'string'],
            [['name', 'password'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'iduser' => 'Iduser',
            'name' => 'Name',
            'password' => 'Password',
            'rule' => 'Rule',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerabots()
    {
        return $this->hasMany(Perabot::className(), ['iduser' => 'iduser']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperti()
    {
        return $this->hasOne(Properti::className(), ['iduser' => 'iduser']);
    }

    public function getAuthKey(): string {
        return 'hgyguygy';
    }

    public function getId() {
        return $this->iduser;
    }

    public function validateAuthKey($authKey): bool {
        return true;
    }
    
    public function validatePassword($password) {
        return $this->password == $password;
        
    }

    public static function findIdentity($id): \yii\web\IdentityInterface {
        return User::find()->where(['iduser'=>$id])->one();
    }

    public static function findIdentityByAccessToken($token, $type = null): \yii\web\IdentityInterface {
        return null;
    }

}
