<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Perabot */

$this->title = 'Update Perabot: '.$model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Perabots', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idperabot, 'url' => ['view', 'id' => $model->idperabot]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="perabot-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
