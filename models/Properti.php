<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "properti".
 *
 * @property int $idproperti
 * @property string $nama
 * @property string $luas
 * @property int $harga
 * @property int $iduser
 *
 * @property Perabot[] $perabots
 * @property User $user
 */
class Properti extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'properti';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'luas', 'harga', 'iduser'], 'required'],
            [['harga', 'iduser'], 'integer'],
            [['nama', 'luas'], 'string', 'max' => 45],
            [['iduser'], 'unique'],
            [['iduser'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['iduser' => 'iduser']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idproperti' => 'Idproperti',
            'nama' => 'Nama',
            'luas' => 'Luas',
            'harga' => 'Harga',
            'iduser' => 'Iduser',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerabots()
    {
        return $this->hasMany(Perabot::className(), ['idproperti' => 'idproperti']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['iduser' => 'iduser']);
    }
}
