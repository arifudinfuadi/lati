<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PerabotSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Perabots';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="perabot-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Perabot', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idperabot',
            'nama',
            'harga',
            'idproperti',
            'iduser',

            ['class' => 'yii\grid\ActionColumn','template' => '{view} {update}{delete}'],
        ],
    ]); ?>
</div>
