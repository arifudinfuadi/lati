<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PropertiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Propertis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="properti-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Properti', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

          //  'idproperti',
            'nama',
            'luas',
            'harga',
            'user.name:ntext:Oleh',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
