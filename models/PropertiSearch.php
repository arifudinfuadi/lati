<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Properti;

/**
 * PropertiSearch represents the model behind the search form of `app\models\Properti`.
 */
class PropertiSearch extends Properti
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idproperti', 'harga', 'iduser'], 'integer'],
            [['nama', 'luas'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Properti::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idproperti' => $this->idproperti,
            'harga' => $this->harga,
            'iduser' => $this->iduser,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'luas', $this->luas]);

        return $dataProvider;
    }
}
